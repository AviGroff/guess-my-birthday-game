

from random import randint

name = input("\nHi! What is your name?\n")


for i in range(5):
    month = randint(1,12)
    year = randint(1924,2004)

    print("\n", i + 1, " : ", name, "were you born in ", month, " / ", year)

    response = input("yes or no?\n")

    if response == "yes":
        exit("\nI knew it!")

    else:
        print("Drat! Lemme try again!")


print("\nI have other things to do. Good bye.")
